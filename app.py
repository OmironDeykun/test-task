from sqlalchemy import create_engine
from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import PrimaryKeyConstraint
from flask import Flask, jsonify, abort, request, make_response


app = Flask(__name__)

db_string = "postgres://postgres:tigr@localhost/temperature_db"

db = create_engine(db_string)
base = declarative_base()

Session = sessionmaker(db)
session = Session()
base.metadata.create_all(db)


class Temperature(base):
    __tablename__ = 'first_test'
    __table_args__ = (
        PrimaryKeyConstraint('id', 'date', 'temperature'),
    )

    id = Column(String)
    date = Column(String)
    temperature = Column(String)


def transform(list):
    result = []
    for moment in list:
        time = {}

        time['id'] = moment.id
        time['date'] = moment.date
        time['temperature'] = moment.temperature
        result.append(time)
    return result


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/')
def main():
    return 'hello, World!'


@app.route('/temperature/<int:moment_id>', methods=['GET'])
def get_moment_temperature(moment_id):
    temperature_list = session.query(Temperature)
    temperature_list = transform(temperature_list)
    for moment in temperature_list:
        if moment['id'] == moment_id:
            return jsonify({'temperature_moment': moment})

    abort(404)


@app.route('/temperature', methods=['GET'])
def get_temperature():
    temperature_list = session.query(Temperature)
    temperature_list = transform(temperature_list)
    return jsonify({'temperature_list': temperature_list})


@app.route('/temperature', methods=['POST'])
def add_temperature():

    print(request.json['some'])
    for day in request.json['some']:
        doctor_strange = Temperature(date=day, temperature=request.json['some'][day])
        session.add(doctor_strange)

    session.commit()

    temperature_list = session.query(Temperature)
    temperature_list = transform(temperature_list)
    return jsonify({'temperature': temperature_list}), 201


if __name__ == '__main__':
    app.run()
